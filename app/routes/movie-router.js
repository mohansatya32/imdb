const express = require('express');
const router = express.Router();
const _ = require('lodash');
const validateUser = require('./../../authentication/validateAuthtoken')
const {Client} = require('elasticsearch')

const esInterface = require('../../app/data-store/elasticReadWriteUtils.js')
const shortid = require('shortid');
const moment = require('moment');
const esQueryForMovies = require('../queryBuilder/getAllMovieNamesQuery')


const authExemptedApi = ['/api/v1/get-movie-names']


router.use((req, res, next) => {
    let excempiton = authExemptedApi.filter(api => {
        let regEx = new RegExp(`^${api}`)
        return req.path.toLowerCase().match(regEx)
    })
    if (_.isEmpty(_.compact(excempiton))) {
        console.log(" go a head without auth")
        return next();
    }
    if (!_.isEmpty(_.get(req, ['rawHeaders', '1']))) {
        console.log("authenticating jwt auth token")
        return next();
    }
    res.send("please login and do updates")
})
router.get('/add-movie', validateUser.validateToken, (req, res) => {
    console.log("hittin add-movie")
    return res.json("users");
});

router.get('/api/v1/movies', validateUser.validateToken, function (req, res) {

    var postBodyPayload = {
        "query": {
            "match_all": {}
        }
    };
    var multiGetUrl = "http://localhost:9200/movies/_search";

    return esInterface.makeHttpCallsWithRetries(multiGetUrl, postBodyPayload, 1, "GET", function (err, results) {
        if (err) {
            console.error("multi get from elastic search failed " + err);
            // return callback(err);
        } else {
            console.debug("successfully retrieved docs from elastic search");
            var resultJson = JSON.parse(results);
            var data
            if (!_.isEmpty(resultJson.hits.hits)) {
                data = JSON.stringify(_.map(resultJson.hits.hits, '_source'))
            } else {
                data = "node rocords found"
            }
            console.log(`list of all movies ${resultJson}`)
            res.status(200).send(`list of all movies ${data}`)
        }
    });
});
let updateRecord = function (req, res, updateAttribute) {
    var doc = [{"_index": "movies", "_id": req.params.id}]
    var postBodyPayload = "{ \"docs\" :" + JSON.stringify(doc) + "}";
    var multiGetUrl = "http://localhost:9200/_mget";

    return esInterface.makeHttpCallsWithRetries(multiGetUrl, postBodyPayload, 1, "POST", function (err, results) {
        if (err) {
            console.error("multi get from elastic search failed " + err);
            return res.status(500)
            //  return callback(err);
        } else {
            console.debug("successfully retrieved docs from elastic search");
            var resultJson = JSON.parse(results);
            console.log(`list of all movies ${resultJson}`)
            var data;
            if (!_.isEmpty(resultJson.docs[0]._source)) {
                data = _.map(_.get(resultJson, 'docs'), '_source')
            } else {
                data = "node rocords found"
                res.status(403).send("no record found")
            }
            let oldRecord = data[0]
            let newRecord
            if (updateAttribute === 'vote') {
                if (_.get(req, ['body', updateAttribute]) === "upvote") {
                    newRecord = _.set(oldRecord.doc, [ "update","vote", "upvote"], _.get(oldRecord, ["doc", "update","vote", "upvote"]) + 1 || 1);
                } else {
                    newRecord = _.set(oldRecord.doc, ["update","vote", "downvote"], _.get(oldRecord, ["doc", "update","vote", "downvote"]) + 1 || 1);
                }
            } else {
                newRecord = _.assign(oldRecord.doc, {
                    [updateAttribute]: _.get(req, ['body', updateAttribute]),
                    last_update_date: moment().format("YYYY/MM/DD HH:mm:ss")
                })

            }


            let newDoc = '{ "index" : {"_index": "movies", "_id" : "' + req.params.id + '"} }\n'
            newDoc += '{"doc" : ' + JSON.stringify(newRecord) + '}' + '\n';
            console.log(newRecord)
            let url = "http://localhost:9200/_bulk?refresh=false"
            return esInterface.makeHttpCallsWithRetries(url, newDoc, 1, "POST", function (err, results) {
                if (err) {
                    console.error("bulkUpdateWorkflows bulk upsert on elastic search failed " + err);
                    return res.status(500)
                }
                console.log(`successfull saved with id :${req.params.id} for vote ${req.body.vote}`)
                return res.status(200).send(`successfull saved with id :${req.params.id} for vote ${req.body.vote}`)
            })
        }
    });
}

router.post('/api/v1/vote/:id', validateUser.validateToken, function (req, res) {
    return updateRecord(req, res, "vote")
})
router.post('/api/v1/genre/:id', validateUser.validateToken,function (req, res) {
    return updateRecord(req, res, "genre")
})
router.post('/api/v1/review/:id', validateUser.validateToken,function (req, res) {
    return updateRecord(req, res, "review")
})


router.get('/api/v1/movie/:id', validateUser.validateToken, function (req, res) {
    var doc = [{"_index": "movies", "_id": req.params.id}]
    var postBodyPayload = "{ \"docs\" :" + JSON.stringify(doc) + "}";
    var multiGetUrl = "http://localhost:9200/_mget";

    esInterface.makeHttpCallsWithRetries(multiGetUrl, postBodyPayload, 1, "POST", function (err, results) {
        if (err) {
            console.error("multi get from elastic search failed " + err);
            //  return callback(err);
        } else {
            console.debug("successfully retrieved docs from elastic search");
            var resultJson = JSON.parse(results);
            console.log(`list of all movies ${resultJson}`)
            var data
            if (!_.isEmpty(resultJson.docs)) {
                data = JSON.stringify(_.map(_.get(resultJson, 'docs'), '_source'))
            } else {
                data = "node rocords found"
            }
            res.status(200).send(`following are details of movies ${JSON.stringify(data)}`)
        }
    });
});

router.post('/api/v1/movie',validateUser.validateToken, function (req, res) {

    let id = shortid.generate();
    var m = {
        id: id,
        title: req.body.title,
        year: req.body.year,
        rated: req.body.rated,
        released_date: moment(req.body.released_date).format("YYYY/MM/DD HH:mm:ss"),
        created_date: moment().format("DD/MM/YYYY HH:mm")
        //user: req.user.user,
    };
    let doc = '{ "index" : {"_index": "movies", "_id" : "' + id + '"} }\n';
    doc += '{"doc" : ' + JSON.stringify(m) + '}' + '\n';
    //logger can be used for better audit
    console.log(doc)
    let url = "http://localhost:9200/_bulk?refresh=false"
    return esInterface.makeHttpCallsWithRetries(url, doc, 1, "POST", function (err, results) {
        if (err) {
            console.error("bulkUpdateWorkflows bulk upsert on elastic search failed " + err);
            res.status(500)
        }
        console.log(`successfull saved with id :${id} for movie ${m.title}`)
        res.status(200).send(`successfull saved with id :${id} for movie ${m.title}`)
    })
});


router.get('/api/v1/get-movie-names', async function (req, res) {
    let query = esQueryForMovies.sortQuery
    const client = new Client({node: 'http://localhost:9200'})
    let xx = await client.search(query)
    let moviesList = []
    _.each(xx.hits.hits, (item) => {
        moviesList.push(item._source.doc)
    })
    console.log("dsfa", _.compact(moviesList))
    res.status(200).send(`movies list ${JSON.stringify(_.compact(moviesList))}`)
});


module.exports = router