var mongoose = require('mongoose');
mongoose.connect("mongodb://localhost:27017/vymo-lms")
var MovieSchema = new mongoose.Schema({
    title: {
        type: String
    },
    year: {
        type: String
    },
    rated: {
        type: String
    },
    runtime: {
        type: String
    },
    user: {
        type: String
    }
});

MovieSchema.set('toJSON', {
    transform: function(doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
    }
});

module.exports = mongoose.model('Movie', MovieSchema);