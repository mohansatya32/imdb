const request = require('request');
const _ = require('lodash');

exports.makeHttpCallsWithRetries = function (postUrl, postBodyJsonString, maxRetries, method, callback) {
    var options = {
        maxRetries: maxRetries,
        postBodyJsonString: postBodyJsonString,
        postUrl: postUrl,
        method: method,
        //5mins
        timeout: 5 * 60 * 1000
    };

    return makeHttpCallsWithOptions(options, callback);
};

let makeHttpCallsWithOptions = function (options,callback) {
    var maxRetries = options.maxRetries,
        postBodyJsonString = options.postBodyJsonString,
        postUrl = options.postUrl,
        method = options.method,
        timeout = options.timeout,
        postHeaders = options.postHeaders || {};
    if (!Number(maxRetries)) {
        maxRetries = 1;
    }


    var errorBody = {};

    function attemptRequest(attempt, callback) {
        if (attempt > maxRetries) {
            console.error("post call to the URL " + postUrl + " with body " + postBodyJsonString + "failed after " + maxRetries + " retries");
            return callback('Failed to make the HTTP request', null, errorBody);
        }
        request({
            url: postUrl,
            method: method,
            headers: _.merge(postHeaders, {
                "content-type": "application/json"
            }),
            qs: method == 'GET' ? postBodyJsonString : '',
            body: method == 'GET' ? '' : postBodyJsonString
        }, function (error, response, body) {
            if (error || (response.statusCode < 200 || response.statusCode > 299)) {
                var status = "Unknown";
                errorBody["error"] = error;
                if (response) {
                    status = response.statusCode;
                    errorBody["response"] = response;
                }
                console.error("post call to the URL " + postUrl + " with body " + JSON.stringify(postBodyJsonString) + "failed for attempt:" + attempt + " Status code" + status + " Error:  " + error + 'error response from rest ::', response);
                return attemptRequest(attempt + 1, callback);
            } else {
                return callback(null, body);
            }
        });
    }

    attemptRequest(1, callback);
};
