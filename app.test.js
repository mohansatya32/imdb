const chai = require('chai')
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const sinon = require('sinon')
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const rewire = require('rewire');
const request = require('supertest');
const express = require('express');

var app = rewire('./app/routes/movie-router.js')
var sandbox = sinon.createSandbox();

describe('app', () => {
    afterEach(() => {
        app = express();
        sandbox.restore();
    })
    context('post /api/v1/movies', () => {
        it('should post /api/v1/movies', async (done) => {
            request(app).post('/api/v1/movies')
                .send({
                    "title": "some thing",
                    "year": "2019",
                    "rated": 8.1,
                    "genre": "horroe",
                    "released_date": "2019/01/01 12:22:11"
                }).set('Accept', 'application/json')
                .expect(201)
                .end((err, response) => {
                    done(err)
                })


        })
    })
})
