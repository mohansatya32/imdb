node version :16.10.0
Database : elasticsearch(version: 7.x)

start app by running `node server.js`

Register user
-------------
To register user,  http call of Post on `/register` api need to be used
example pay load ```{
"name": "mohan",
"password": "qa12ws12@@"
}```


login user
----------
To login with  user credential, http call of Post on `/register/login` api need to be used
example payload 
```
{
"name": "mohan",
"password": "qa12ws12@@"
}
```
which will generate auth-token and refresh token, auth-token is valid up 15 mins


list of movies 
--------------
To Add movies detail for registered user, http call of GET following api `/movies/api/v1/movies` need to be used 
with in the request headers auth token need to be provided with `authorisation`

sample out put of list of  ```{"doc":{"id":"NgfqGErIy","title":"rakshi","year":"2022","rated":8.1,"released_date":"2022/01/01
12:22:11","created_date":"30/01/2022
09:48"}},{"doc":{"id":"2RnQcxuj2","title":"raj","year":"2022","rated":2,"released_date":"2022/01/01
12:22:11","created_date":"30/01/2022 08:36","update":{"vote":{"downvote":5,"upvote":4}},"last_update_date":"2022/01/30
09:42:37","review":"asfsdfasd"}}]```


Add movie 
--------------
To Add movies detail for registered user, http call of POST following api `/movies/api/v1/movie`
```{
    "title": "rakshi",
    "year": "2022",
    "rated": 8.1,
    "genre": "suspense",
    "released_date": "2022/01/01 12:22:11"
}

```
as a meta data created_date of record will ge added in DB
in the request headers auth token need to be provided with `authorisation`

update movie with genre,review,vote
------------------------------------
 as a meta data created updated_date of record will ge added in DB and auth token need to be added 
 with key authorisation in header for request

 vote 
 ----
 http method of post need to be used on api `/movies/api/v1/vote/:id`
 example api call `/movies/api/v1/vote/2RnQcxuj2` pay load
  ```{
    "vote" : "upvote"
  }
  ```
  which increses count by 1 for upvote and downvote in db based on current count 

   genre
   ----
   http method of post need to be used on api `/movies/api/v1/genre/:id`
   example api call `/movies/api/v1/genre/2RnQcxuj2` pay load
  ```{
    "genre" : "Action"
  }
  ```

    review 
    ----
   http method of post need to be used on api `/movies/api/v1/review/:id`
   example api call `/movies/api/v1/review/2RnQcxuj2` pay load

  ```{
    "review" : "entertainment is good"
  }
  ```

Get Movie names
-----------
public api `/movies/api/v1/get-movie-names` call with http method GET give list of movies 
sorted on rating, upvote and released date in descending order
 sample  output is 
```[{"rated":10,"title":"raj"},{"rated":8.1,"title":"rakshi"},{"rated":8.1,"title":"rakshi"},{"rated":3,"title":"pushpa"},{"rated":2,"title":"raj"}]```

libraries used
----------
bcrypt : to apply hash on passwod
express : for routing api requests
request : to make hhtp request
moment : to genenate dat in given format
shortid : to generate  unique short id for assigning to document in db
jsonwebtoken : to genrate auth-token and request-token for validating  user requests



